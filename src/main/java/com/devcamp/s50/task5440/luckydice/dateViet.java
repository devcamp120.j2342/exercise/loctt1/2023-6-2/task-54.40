package com.devcamp.s50.task5440.luckydice;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@RestController
@CrossOrigin
public class dateViet {
     @GetMapping("/devcamp-welcome/date-viet")
     public String getDateViet(){
          DateTimeFormatter dateViet = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
          LocalDate today = LocalDate.now(ZoneId.systemDefault());
          return String.format("Hello pizza lover! Hôm nay %s mua 1 tặng 1.", dateViet.format(today));
     }

}
