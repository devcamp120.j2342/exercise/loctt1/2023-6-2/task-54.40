package com.devcamp.s50.task5440.luckydice;


import java.util.Random;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class LuckyDice {
     @GetMapping("/devcamp-welcome/lucky-dice")
     public String sayHello(@RequestParam String username, @RequestParam String firstname, @RequestParam String lastname){
          int luckyNumber = generateLuckyNumber();
          return "Xin chào, " + username + ". Số may mắn hôm nay của bạn là:" + luckyNumber;
          
     }

     private int generateLuckyNumber() {
          Random random = new Random();
          return random.nextInt(6)+1;
     }

}
